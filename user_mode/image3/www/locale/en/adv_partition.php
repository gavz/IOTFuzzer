<?
$m_context_title = "WLAN Partition";
$m_isc = "Internal Station Connection";
$m_guest = "Guest mode";
$m_ewa = "Ethernet to WLAN Access";
$m_enable = "Enable";
$m_disable = "Disable";
$m_band = "Wireless Band";
$m_band_5G = "5GHz";
$m_band_2.4G = "2.4GHz";
$m_link_integrality="Link Integrity";
$m_pri_ssid = "Primary SSID";
$m_ms_ssid1 = "Multi-SSID 1";
$m_ms_ssid2 = "Multi-SSID 2";
$m_ms_ssid3 = "Multi-SSID 3";
$m_ms_ssid4 = "Multi-SSID 4";
$m_ms_ssid5 = "Multi-SSID 5";
$m_ms_ssid6 = "Multi-SSID 6";
$m_ms_ssid7 = "Multi-SSID 7";
$m_ms_ssid8 = "Multi-SSID 8";
$m_ms_ssid9 = "Multi-SSID 9";
$m_ms_ssid10 = "Multi-SSID 10";
$m_ms_ssid11 = "Multi-SSID 11";
$m_ms_ssid12 = "Multi-SSID 12";
$m_ms_ssid13 = "Multi-SSID 13";
$m_ms_ssid14 = "Multi-SSID 14";
$m_ms_ssid15 = "Multi-SSID 15";


$a_will_block_packets = "AP will block all multicast/broadcast packets from Ethernet to Wireless, except DHCP's.";
?>
